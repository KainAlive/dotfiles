# My Dotfiles
Dotfiles for some programs I use on an daily bases.
Included are configuration files for:
- Neovim (nvim folder)
- Kitty (kitty folder)

## How to setup?
1. Clone the repository
```bash
git clone --recurse-submodules https://gitlab.com/KainAlive/dotfiles.git ~/dotfiles
```
2. Create links 
- Neovim:
```bash
ln -s ~/dotfiles/nvim ~/.config/nvim
```
- Kitty
```bash
ln -s ~/dotfiles/kitty ~/.config/kitty
```
3. Enjoy :)
