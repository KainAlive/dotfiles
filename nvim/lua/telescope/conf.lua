local map = vim.api.nvim_set_keymap

-- SPACE as leader key
map('n', '<Space>', '', {})
vim.g.mapleader = ' '  -- 'vim.g' sets global variables

-- Keybindings for telescope
options = { noremap = true, silent = true}
map('n', '<leader>ff', [[<Cmd>lua require('telescope.builtin').find_files()<CR>]], options)
map('n', '<leader>fg', [[<Cmd>lua require('telescope.builtin').live_grep()<CR>]], options)

-- Load telescope-fzf for speed
--require('telescope').load_extension('fzf')
