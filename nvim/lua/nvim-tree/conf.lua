local map = vim.api.nvim_set_keymap

require('nvim-tree').setup()

map('n', '<A-t>', ':NvimTreeToggle<CR>', {noremap = true, silent = true})
