-- Packer plugin manager

return require('packer').startup(function (use)
    -- ###### EDITOR PLUGINS ######
    -- Packer manages itself
    use 'wbthomason/packer.nvim'
    -- Lualine statusline
    use {
        'nvim-lualine/lualine.nvim',
        requires = {'kyazdani42/nvim-web-devicons'}
    }
    -- LuaTree file manager - NERDTree alternative
    use {
        'kyazdani42/nvim-tree.lua',
        requires = {'kyazdani42/nvim-web-devicons'}
    }
    -- Polyglot
    use 'sheerun/vim-polyglot'
    -- Telescope fuzzy finder
    use {
        'nvim-telescope/telescope.nvim',
        requires = {{'nvim-lua/plenary.nvim'}, {'nvim-telescope/telescope-fzf-native.nvim'}, run = 'make'}
    }
    -- OneDarkPro colot scheme
    use 'olimorris/onedarkpro.nvim'

    -- Dashboard
    use 'glepnir/dashboard-nvim'
    -- Move lines
    use 'fedepujol/move.nvim'


    -- ###### LANGUAGE PLUGINS ######
    -- Coc LSP
    use {
        'neoclide/coc.nvim',
        branch = 'release',
    }
    -- Kitty syntax highlight
    use 'fladson/vim-kitty'
    -- Vim Golang support
    use {
        'fatih/vim-go',
        run = ':GoUpdateBinaries'
    }
    -- LaTeX
    use 'lervag/vimtex'

    -- Hare
    use 'https://git.sr.ht/~sircmpwn/hare.vim'
    -- V-Lang
    use 'https://github.com/ollykel/v-vim'
    -- ###### UTILS #######
    -- Refactoring
    use {
        "ThePrimeagen/refactoring.nvim",
        requires = {
            {"nvim-lua/plenary.nvim"},
            {"nvim-treesitter/nvim-treesitter"}
        }
    }


end)
