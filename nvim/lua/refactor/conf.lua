local map = vim.api.nvim_set_keymap

require('refactoring').setup({})

map(
    "v",
    "<leader>rr",
    ":lua require('refactoring').select_refactor()<CR>",
    { noremap = true, silent = true, expr = false }
)
